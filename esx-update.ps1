########################################
# PowerCLI Script to Patch ESXi Hosts
# Created by kurt@giessel.net
########################################

# Select which vCenter you want to connect to
# replace vc# with your VC Servers
Write-host "Select which vCenter to connect to:"
Write-Host ""
Write-Host "1. vc1"
Write-Host "2. vc2"
Write-Host "3. vc3"

$Ivcenter = read-host "Select a vCenter Server (enter number)"

# replace vc#-url with the fqdn of your vc servers
if ($Ivcenter -eq 1) {
  $vcenter = "vc1-url"
} elseif ($Ivcenter -eq 2) {
  $vcenter = "vc2-url"
} else {
  $vcenter = "vc3-url"
}

write-host ""
Write-Host "You Picked: "$vcenter
write-host ""
start-sleep -s 3

# connect to selected vCenter
connect-viserver $vcenter

# List Clusters
Write-host "Choose which Cluster the host you want to patch belongs to"
write-host ""
$ICLUSTER = get-cluster | Select Name | Sort-object Name
$i = 1
$ICLUSTER | %{Write-Host $i":" $_.Name; $i++}
$HCLUSTER = Read-host "Enter the number for the cluster"
$SCLUSTER = $ICLUSTER[$HCLUSTER -1].Name
write-host "you have selected" $SCLUSTER"."
start-sleep -s 3

# List hosts to select
write-host ""
Write-host "Choose which vSphere host in $($SCLUSTER) to Deploy Patches to"
$IHOST = Get-View -ViewType HostSystem -Property Name,Config.Product | Sort-object Name
#$IHOST = Get-Cluster $SCLUSTER | Get-VMhost | Select Name | Sort-object Name
$i = 1
$IHOST | Format-Table Name, @{L='Host Version & Build Version';E={$_.Config.Product.FullName}}
$DSHost = Read-host "Enter the number for the host to patch"
$SHOST = $IHOST[$DSHost -1].Name
write-host "you have selected $($SHOST)"

# Scan selected host
test-compliance -entity $SHOST

# Display compliance results and wait 15 seconds
get-compliance -entity $SHOST
start-sleep -s 15

# Place selected host into Maintenance mode
write-host "Placing $($SHOST) in Maintenance Mode"
Get-VMHost -Name $SHOST | set-vmhost -State Maintenance

# Remediate selected host for Host Patches
write-host "Deploying VMware Host Critical &amp; Non Critical Patches"
get-baseline -name *critical* | update-entity -entity $SHOST -confirm:$false

# Remove selected host from Maintenance mode
write-host "Removing host from Maintenance Mode"
Get-VMHost -Name $SHOST | set-vmhost -State Connected

# Display Popup when finished.
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[System.Windows.Forms.MessageBox]::Show("The Patching for $($SHOST) is now complete...")